package br.com.pos.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pos.dto.MovimentDTO;
import br.com.pos.model.Moviment;
import br.com.pos.service.MovimentService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping(value ="/v1/order-moviment", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OrderMovimentController {

	private final MovimentService movimentService;
	
	@ApiOperation(value = "List All order moviments", response = Moviment[].class)
	@GetMapping
	public List<MovimentDTO> findAll() {
		return movimentService.findAll();
	}
	
	@ApiOperation(value = "Save or Update order moviment", response = MovimentDTO.class)
	@PostMapping("/save-moviment")
	public MovimentDTO createMoviment(@RequestBody MovimentDTO movimentDTO) {
		return movimentService.saveOrderMoviment(movimentDTO);
	}
}
