package br.com.pos.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pos.dto.ProductDTO;
import br.com.pos.model.Product;
import br.com.pos.service.ProductService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping(value ="/v1/products", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ProductController {

	private final ProductService productService;
	
	@ApiOperation(value = "List All products", response = Product[].class)
	@GetMapping
	public List<ProductDTO> findAll() {
		return productService.findAll();
	}
}