package br.com.pos.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pos.dto.ClientDTO;
import br.com.pos.model.Client;
import br.com.pos.service.ClientService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping(value ="/v1/clients", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ClientController {
	
	private final ClientService clientService;
	
	@ApiOperation(value = "List All clients", response = Client[].class)
	@GetMapping
	public List<ClientDTO> findAll() {
		return clientService.findAll();
	}
	
	@ApiOperation(value = "Save or Update a client", response = ClientDTO.class)
	@PostMapping("/save-client")
	public ClientDTO createClient(@RequestBody ClientDTO clientDTO) {
		return clientService.saveClient(clientDTO);
	}
	
	@ApiOperation(value = "Inativate a client")
	@GetMapping("/inativate-client/{id}")
	public void inativateClient(@PathVariable("id") Long id) {
		clientService.inativateClient(id);
	}
}
