package br.com.pos.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pos.dto.PaymentMethodDTO;
import br.com.pos.model.PaymentMethod;
import br.com.pos.service.PaymentMethodService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping(value ="/v1/payment_methods", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PaymentMethodController {
	
	private final PaymentMethodService paymentMethodService;
	
	@ApiOperation(value = "Find all payment methods", response = PaymentMethod[].class)
	@GetMapping
	public List<PaymentMethodDTO> findAll() {
		return paymentMethodService.findAll();
	}
	
}
