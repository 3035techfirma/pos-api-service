package br.com.pos.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.pos.dto.ClientDTO;
import br.com.pos.model.Client;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ClientConverter implements ConvertObject<ClientDTO, Client>{
	
	@Override
	public ClientDTO convertModelToDto(Client model) {
		return ClientDTO.builder().id(model.getId()).name(model.getName()).email(model.getEmail())
				.documentType(model.getDocumentType()).document(model.getDocument())
				.phone(model.getPhone()).gender(model.getGender()).status(model.getStatus())
				.tagList(model.getTagList()).vendorId(model.getSpreeVendorId()).build();
	}

	@Override
	public List<ClientDTO> convertListModelToListDto(List<Client> models) {
		return models.stream().map(model -> this.convertModelToDto(model)).collect(Collectors.toList());
	}

	@Override
	public Client convertDtoToModel(ClientDTO dto) {
		return Client.builder().id(dto.getId()).name(dto.getName()).email(dto.getEmail())
				.documentType(dto.getDocumentType()).document(dto.getDocument())
				.phone(dto.getPhone()).gender(dto.getGender()).status(dto.getStatus())
				.tagList(dto.getTagList()).spreeVendorId(dto.getVendorId()).build();
	}

	@Override
	public List<Client> convertListDtoToListModel(List<ClientDTO> dtos) {
		return dtos.stream().map(dto -> this.convertDtoToModel(dto)).collect(Collectors.toList());
	}
	

}
