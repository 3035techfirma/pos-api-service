package br.com.pos.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.pos.dto.MovimentDTO;
import br.com.pos.model.Moviment;
import br.com.pos.model.PaymentMethod;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class PaymentMovimentConverter implements ConvertObject<MovimentDTO, Moviment> {
	
	@Override
	public MovimentDTO convertModelToDto(Moviment model) {
		if(model.getObject() != null) {
			//PaymentMethod payment = (PaymentMethod) model.getObject();
			return MovimentDTO.builder().id(model.getId()).action(model.getAction()).objectId(model.getId())
					.value(model.getValue()).createdAt(model.getCreatedAt()).build();
		} else return MovimentDTO.builder().id(model.getId()).action(model.getAction())
				.value(model.getValue()).createdAt(model.getCreatedAt()).build();
	}

	@Override
	public List<MovimentDTO> convertListModelToListDto(List<Moviment> models) {
		return models.stream().map(model -> this.convertModelToDto(model)).collect(Collectors.toList());
	}

	@Override
	public Moviment convertDtoToModel(MovimentDTO dto) {
		PaymentMethod payment = new PaymentMethod();
		payment.setId(dto.getObjectId());
		return Moviment.builder().id(dto.getId()).action(dto.getAction()).object(dto.getObjectId())
				.value(dto.getValue()).createdAt(dto.getCreatedAt()).build();
	}

	@Override
	public List<Moviment> convertListDtoToListModel(List<MovimentDTO> dtos) {
		return dtos.stream().map(dto -> this.convertDtoToModel(dto)).collect(Collectors.toList());
	}	
}

