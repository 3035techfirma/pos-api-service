package br.com.pos.converter;

import java.util.List;

public interface ConvertObject<D, M> {
	
	public abstract D convertModelToDto(M model);

	public abstract List<D> convertListModelToListDto(List<M> models);

	public abstract M convertDtoToModel(D dto);

	public abstract List<M> convertListDtoToListModel(List<D> dtos);
}
