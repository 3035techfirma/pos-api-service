package br.com.pos.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.pos.dto.PaymentMethodDTO;
import br.com.pos.model.PaymentMethod;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class PaymentMethodConverter implements ConvertObject<PaymentMethodDTO, PaymentMethod>{
	
	@Override
	public PaymentMethodDTO convertModelToDto(PaymentMethod model) {
		return PaymentMethodDTO.builder().id(model.getId()).cardBrand(model.getName()).installments(checkIfNumeric(model.getDescription())).build();
	}

	@Override
	public List<PaymentMethodDTO> convertListModelToListDto(List<PaymentMethod> models) {
		return models.stream().map(model -> this.convertModelToDto(model)).collect(Collectors.toList());
	}

	@Override
	public PaymentMethod convertDtoToModel(PaymentMethodDTO dto) {
		return PaymentMethod.builder().id(dto.getId()).name(dto.getCardBrand()).description(dto.getInstallments().toString()).build();
	}

	@Override
	public List<PaymentMethod> convertListDtoToListModel(List<PaymentMethodDTO> dtos) {
		return dtos.stream().map(dto -> this.convertDtoToModel(dto)).collect(Collectors.toList());
	}
	
	private Integer checkIfNumeric(String string) {
		if(!string.matches("-?\\d+(\\.\\d+)?")) 
			return 0;
		else
			return Integer.parseInt(string);
	}
}
