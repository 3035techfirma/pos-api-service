package br.com.pos.converter;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.pos.dto.ProductDTO;
import br.com.pos.model.Product;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ProductConverter implements ConvertObject<ProductDTO, Product>{
	
	@Override
	public ProductDTO convertModelToDto(Product model) {
		return ProductDTO.builder().id(model.getId()).name(model.getName()).description(model.getDescription()).build();
	}

	@Override
	public List<ProductDTO> convertListModelToListDto(List<Product> models) {
		return models.stream().map(model -> this.convertModelToDto(model)).collect(Collectors.toList());
	}

	@Override
	public Product convertDtoToModel(ProductDTO dto) {
		return Product.builder().id(dto.getId()).name(dto.getName()).description(dto.getDescription()).build();
	}

	@Override
	public List<Product> convertListDtoToListModel(List<ProductDTO> dtos) {
		return dtos.stream().map(dto -> this.convertDtoToModel(dto)).collect(Collectors.toList());
	}
	
}