package br.com.pos.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.pos.domain.OrderState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "spree_orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private OrderState state;
	
	@Column(columnDefinition = "Decimal(10,2) default '0.0'")
	private BigDecimal itemTotal;
	@Column(columnDefinition = "Decimal(10,2) default '0.0'")
	private BigDecimal total;
	@Column(columnDefinition = "Decimal(10,2) default '0.0'")
	private BigDecimal paymentTotal;
	@Column(columnDefinition = "Decimal(10,2) default '0.0'")
	private BigDecimal promoTotal;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDateTime createdAt;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDateTime updatedAt;
}
