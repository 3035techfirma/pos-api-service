package br.com.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pos.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
