package br.com.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pos.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {

}
