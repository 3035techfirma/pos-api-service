package br.com.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pos.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long>{
	
}
