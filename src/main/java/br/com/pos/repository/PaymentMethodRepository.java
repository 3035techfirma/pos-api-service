package br.com.pos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pos.model.PaymentMethod;

public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Long> {
	public List<PaymentMethod> findByOrderByNameAsc();
}
