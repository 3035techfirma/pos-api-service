package br.com.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pos.model.PosOrder;

public interface PosOrderRepository extends JpaRepository<PosOrder, Long> {

}
