package br.com.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pos.model.LineItem;

public interface LineItemRepository extends JpaRepository<LineItem, Long> {

}
