package br.com.pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pos.model.Moviment;

public interface MovimentRepository  extends JpaRepository<Moviment, Long>{

}
