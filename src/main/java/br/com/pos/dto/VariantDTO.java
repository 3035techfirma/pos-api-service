package br.com.pos.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VariantDTO {
	
	private Long id;
	private Long productId;
	private String sku;

}
