package br.com.pos.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemDTO {

	private Long id;	
	private String sku;
	private Integer quantity;
	private Double unitaryValue;
	private Double totalValue;
	private Double discount;
}
