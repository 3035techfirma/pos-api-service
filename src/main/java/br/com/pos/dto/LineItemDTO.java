package br.com.pos.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LineItemDTO {
	
	private Long id;
	private Long orderId;
	private Long variantId;
	private BigDecimal price;
	private Integer quantity;
}
