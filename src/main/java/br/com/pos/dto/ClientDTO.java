package br.com.pos.dto;

import br.com.pos.domain.DocumentType;
import br.com.pos.domain.Gender;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientDTO {

	private Long id;
	private String name;
	private DocumentType documentType;
	private String document;
	private String phone;
	private String email;
	private Boolean status;
	private Gender gender;
	private Long vendorId;
	private String tagList;
}
