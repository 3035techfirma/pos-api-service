package br.com.pos.dto;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MovimentDTO {

	private Long id;
	private char action;
	private Long objectId;
	private Double value;
	private LocalDateTime createdAt;
}
