package br.com.pos.dto;

import br.com.pos.domain.PosOrderStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PosOrderDTO {
	
	private Long id;
	private PosOrderStatus status;

}
