package br.com.pos.dto;

import java.math.BigDecimal;

import br.com.pos.domain.OrderState;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderDTO {

	private Long id;
	private OrderState state;
	private BigDecimal total;
	private BigDecimal itemTotal;
	
	
}
