package br.com.pos.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentMethodDTO {
	
	private Long id;
	private String cardBrand;
	private Integer installments;

}
