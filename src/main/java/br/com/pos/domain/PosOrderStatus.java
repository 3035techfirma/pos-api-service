package br.com.pos.domain;

public enum PosOrderStatus {
	
	CREATED(1), PENDING(2), DONE(3);
	
	Integer code;
	
	private PosOrderStatus(Integer code) {
		this.code = code;
	}
}
