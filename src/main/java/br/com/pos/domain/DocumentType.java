package br.com.pos.domain;

public enum DocumentType {
	
	CPF(1), CNPJ(2), RG(3);
	
	Integer code;
	
	DocumentType(Integer code) {
		this.code = code;
	}
}
