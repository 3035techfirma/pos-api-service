package br.com.pos.domain;

public enum Gender {
	
	MALE(1), FEMALE(2), OTHER(3);
	
	Integer code;
	
	Gender(Integer code) {
		this.code = code;
	}
}
