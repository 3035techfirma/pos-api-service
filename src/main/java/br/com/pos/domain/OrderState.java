package br.com.pos.domain;

public enum OrderState {
	CART, COMPLETE;
}
