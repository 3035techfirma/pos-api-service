package br.com.pos.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.pos.converter.ProductConverter;
import br.com.pos.dto.ProductDTO;
import br.com.pos.repository.ProductRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ProductService {

	private final ProductRepository productRepository;
	private final ProductConverter productConverter;
	
	public List<ProductDTO> findAll() {
		return productConverter.convertListModelToListDto(productRepository.findAll()); 
	}
}
