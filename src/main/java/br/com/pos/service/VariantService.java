package br.com.pos.service;

import org.springframework.stereotype.Service;

import br.com.pos.repository.VariantRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class VariantService {
	
	private final VariantRepository variantRepository;
}
