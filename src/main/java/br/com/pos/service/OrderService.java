package br.com.pos.service;

import org.springframework.stereotype.Service;

import br.com.pos.repository.OrderRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class OrderService {

	private OrderRepository orderRepository;
}
