package br.com.pos.service;

import org.springframework.stereotype.Service;

import br.com.pos.repository.LineItemRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class LineItemService {
	
	private final LineItemRepository lineItemRepository;
}
