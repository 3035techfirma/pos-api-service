package br.com.pos.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.pos.converter.OrderMovimentConverter;
import br.com.pos.dto.MovimentDTO;
import br.com.pos.model.Moviment;
import br.com.pos.repository.MovimentRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MovimentService {

	private final MovimentRepository movimentRepository;
	private final OrderMovimentConverter movimentConverter;
	
	public List<MovimentDTO> findAll() {
		return movimentConverter.convertListModelToListDto(movimentRepository.findAll()); 
	}
	
	public MovimentDTO saveOrderMoviment(MovimentDTO movimentDTO) {
		Moviment moviment = movimentConverter.convertDtoToModel(movimentDTO);
		
		LocalDateTime date = LocalDateTime.now();
		
		if(moviment.getId() == null) {
			moviment.setCreatedAt(date);
		}
		
		Moviment movimentCreated = movimentRepository.save(moviment);
		return movimentConverter.convertModelToDto(movimentCreated);
	}
	
	public MovimentDTO savePaymentMoviment(MovimentDTO movimentDTO) {
		Moviment moviment = movimentConverter.convertDtoToModel(movimentDTO);
		
		LocalDateTime date = LocalDateTime.now();
		
		if(moviment.getId() == null) {
			moviment.setCreatedAt(date);
		}
		
		Moviment movimentCreated = movimentRepository.save(moviment);
		return movimentConverter.convertModelToDto(movimentCreated);
	}
}
