package br.com.pos.service;

import org.springframework.stereotype.Service;

import br.com.pos.repository.PosOrderRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PosOrderService {

	private final PosOrderRepository posOrderRepository;
}
