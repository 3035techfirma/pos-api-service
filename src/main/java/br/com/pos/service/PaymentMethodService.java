package br.com.pos.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.pos.converter.PaymentMethodConverter;
import br.com.pos.dto.PaymentMethodDTO;
import br.com.pos.model.PaymentMethod;
import br.com.pos.repository.PaymentMethodRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PaymentMethodService {

	private final PaymentMethodRepository paymentMethodRepository;
	private final PaymentMethodConverter paymentMethodConverter;
	
	public List<PaymentMethodDTO> findAll() {
		List<PaymentMethod> list  = paymentMethodRepository.findByOrderByNameAsc();
		
		List<PaymentMethod> paymentMethodsList = new ArrayList<PaymentMethod>();
		
		for (PaymentMethod paymentMethod : list) {
			if(paymentMethod.getDeletedAt() == null) {
				paymentMethodsList.add(paymentMethod);
			}
		}
		
		return paymentMethodConverter.convertListModelToListDto(paymentMethodsList);
	}
	
	
}
