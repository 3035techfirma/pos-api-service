package br.com.pos.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.pos.converter.ClientConverter;
import br.com.pos.dto.ClientDTO;
import br.com.pos.model.Client;
import br.com.pos.repository.ClientRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ClientService {

	private final ClientRepository clientRepository;
	private final ClientConverter clientConverter;
	
	public List<ClientDTO> findAll() {
		return clientConverter.convertListModelToListDto(clientRepository.findAll()); 
	}

	public ClientDTO saveClient(ClientDTO clientDTO) {
		Client client = clientConverter.convertDtoToModel(clientDTO);
		
		LocalDateTime date = LocalDateTime.now();
		
		if(client.getId() == null) {
			client.setCreatedAt(date);
		}
		
		client.setUpdatedAt(date);
		
		Client clientCreated = clientRepository.save(client);
		return clientConverter.convertModelToDto(clientCreated);
	}
	
	public void inativateClient(Long id) {
		Client client = clientRepository.getOne(id);
		client.setStatus(false);
		clientRepository.save(client);
	}
}
